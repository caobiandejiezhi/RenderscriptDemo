package com.example.lincoln.renderscriptdemo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private Button mButton;
    private Button mButton2;
    private SeekBar mSeekBar;
    private SeekBar mSeekBar2;
    private ImageView mImageView1;
    private Bitmap mBitmap;
    private int mCompress = 1;
    private int mRadius = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
    }

    private void initView() {
        mButton = (Button) findViewById(R.id.btn_init);
        mButton.setOnClickListener(this);
        mButton2 = (Button) findViewById(R.id.btn_back);
        mButton2.setOnClickListener(this);
        mImageView1 = (ImageView) findViewById(R.id.img1);
        mSeekBar = (SeekBar) findViewById(R.id.seekbar_co);
        mSeekBar.setOnSeekBarChangeListener(this);
        mSeekBar2 = (SeekBar) findViewById(R.id.seekbar);
        mSeekBar2.setOnSeekBarChangeListener(this);
    }

    private void initBtmip(int compress) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        // 设置它的压缩率，表示压缩1/2  配合压缩图片模糊效率更高
        options.inSampleSize = compress;
        mBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.bg, options);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_init:
                initBtmip(4);
                RenderscriptUtil.blurBitmap(this, mBitmap, 25f, new RenderscriptUtil.CallbackListener() {
                    @Override
                    public void onBitmapResponse(Bitmap bitmap) {
                        mImageView1.setImageBitmap(bitmap);
                    }
                });
                break;
            case R.id.btn_back:
                mBitmap = null;
                mImageView1.setImageResource(R.drawable.bg);
                mSeekBar.setProgress(0);
                mSeekBar2.setProgress(0);
            default:
                break;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mBitmap = null;
        switch (seekBar.getId()) {
            case R.id.seekbar_co:
                mCompress = progress;
                break;
            case R.id.seekbar:
                if (progress < 4) {
                    mRadius = 1;
                } else {
                    mRadius = progress/4;
                }
                break;

            default:
                break;
        }
        initBtmip(mCompress);
        RenderscriptUtil.blurBitmap(this, mBitmap, mRadius, new RenderscriptUtil.CallbackListener() {
            @Override
            public void onBitmapResponse(Bitmap bitmap) {
                mImageView1.setImageBitmap(bitmap);
            }
        });

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

}
